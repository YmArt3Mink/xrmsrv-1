﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace MRXSRV1
{
    [ServiceContract]
    public interface IBookService
    {
        [OperationContract(Name = "GetById")]
        Book Get(int id);

        [OperationContract(Name = "GetByAuthorName")]
        Book[] Get(string author);
        
        [OperationContract]
        void Add(Book book);

        [OperationContract]
        Book Rent(Book book);

        [OperationContract]
        void GiveBack(Book book);
    }

    [DataContract]
    public enum PublicationType
    {
        [EnumMember]
        Literature,

        [EnumMember]
        Magazine,

        [EnumMember]
        Journal,

        [EnumMember]
        Comicbook
    }

    [DataContract]
    public class Book 
        : IEquatable<Book>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public uint Year { get; set; }

        [DataMember]
        public PublicationType PublicationType { get; set; }

        public bool IsRented { get; set; }

        public bool Equals(Book other)
        {
            return
                (Id == other.Id)
                || (Name.Equals(other.Name)
                    && Author.Equals(other.Author)
                    && Year == other.Year
                    && PublicationType == other.PublicationType);
        }
    }
}
