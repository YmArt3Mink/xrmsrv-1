﻿using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

namespace MRXSRV1
{
    public class BookService : IBookService
    {
        private static ICollection<Book> _books
            = new List<Book>();

        public BookService()
        {
            _books.Add(new Book
            {
                Id = 1,
                Name = "Name_1",
                Author = "Author_1",
                PublicationType = PublicationType.Literature,
                Year = 1998
            });
            _books.Add(new Book
            {
                Id = 2,
                Name = "Name_1",
                Author = "Author_2",
                PublicationType = PublicationType.Magazine,
                Year = 1986
            });
            _books.Add(new Book
            {
                Id = 1,
                Name = "Name_2",
                Author = "Author_1",
                PublicationType = PublicationType.Literature,
                Year = 2004
            });
        }

        public Book Get(int id)
        {
            Debug.WriteLine("Get (by Id)");
            return _books.First(i => i.Id == id);
        }

        public Book[] Get(string author)
        {
            Debug.WriteLine("Get (by Author)");
            return
                _books
                    .Where(i => i.Author == author)
                    .ToArray();
        }

        public void Add(Book book)
        {
            Debug.WriteLine("Add");
            _books.Add(book);
        }

        public Book Rent(Book book)
        {
            Debug.WriteLine("Take");
            var result = _books.First(i => i.Equals(book) && !i.IsRented);
            result.IsRented = true;
            
            return result;
        }

        public void GiveBack(Book book)
        {
            Debug.WriteLine("GiveBack");
            var result = _books.First(i => i.Equals(book) && i.IsRented);
            result.IsRented = false;
        }
    }
}
